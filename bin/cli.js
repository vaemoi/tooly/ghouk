#!/usr/bin/env node

const { parse } = require(`cli-flags`);

const GhoukOpts = require(`../lib/ghouk-options.js`);

const { flags } = parse(GhoukOpts);

if (flags.version) {
  console.log(`ghouk version: ${require(`../package.json`).version}`);
} else {
  console.log(`Coming soon`);
}

