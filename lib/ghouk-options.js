const { flags: Flags } = require(`cli-flags`);

module.exports = {
  flags: {
    version: Flags.boolean({
      char: `v`,
      description: `Show the current version of orpin-cli installed`
    })
  }
};
